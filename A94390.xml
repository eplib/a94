<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A94390">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To all and every the constables of St. Clements Danes of the Dutchy Liberty, of Covent-garden, and St. Martins in the Fields</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A94390 of text R210254 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.27[6]). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A94390</idno>
    <idno type="STC">Wing T1319</idno>
    <idno type="STC">Thomason 669.f.27[6]</idno>
    <idno type="STC">ESTC R210254</idno>
    <idno type="EEBO-CITATION">99869069</idno>
    <idno type="PROQUEST">99869069</idno>
    <idno type="VID">170703</idno>
    <idno type="PROQUESTGOID">2240929536</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A94390)</note>
    <note>Transcribed from: (Early English Books Online ; image set 170703)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 256:669f27[6])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To all and every the constables of St. Clements Danes of the Dutchy Liberty, of Covent-garden, and St. Martins in the Fields</title>
      <author>Throckmorton, William.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by John Bill, Printer to the King's most Excellent Majesty,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1661.</date>
     </publicationStmt>
     <notesStmt>
      <note>An order of the Knight Marshall respecting regulations to be observed at the King's coronation.</note>
      <note>Dated and signed at end: Whitehall, by the authority above named, the eight day of April, one thousand six hundred sixty one. William Throckmorton.</note>
      <note>Annotation on Thomason copy: "April 9".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Charles -- II, -- King of England, 1630-1685 -- Coronation -- Early works to 1800.</term>
     <term>Coronations -- Great Britain -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>To all and every the constables of St. Clements Danes of the Dutchy Liberty, of Covent-garden, and St. Martins in the Fields.</ep:title>
    <ep:author>Throckmorton, William. </ep:author>
    <ep:publicationYear>1661</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>389</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-06</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-07</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-08</date>
    <label>Jonathan Blaney</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-08</date>
    <label>Jonathan Blaney</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A94390-t">
  <body xml:id="A94390-e0">
   <div type="text" xml:id="A94390-e10">
    <pb facs="tcp:170703:1" rend="simple:additions" xml:id="A94390-001-a"/>
    <head xml:id="A94390-e20">
     <w lemma="to" pos="prt" xml:id="A94390-001-a-0010">To</w>
     <w lemma="all" pos="d" xml:id="A94390-001-a-0020">all</w>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-0030">and</w>
     <w lemma="every" pos="d" xml:id="A94390-001-a-0040">every</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-0050">the</w>
     <w lemma="constable" pos="n2" xml:id="A94390-001-a-0060">Constables</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-0070">of</w>
     <w lemma="st." pos="ab" xml:id="A94390-001-a-0080">St.</w>
     <hi xml:id="A94390-e30">
      <w lemma="Clement" pos="nng1" reg="Clement's" xml:id="A94390-001-a-0090">Clements</w>
      <w lemma="Danes" pos="nn1" xml:id="A94390-001-a-0100">Danes</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-0110">of</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-0120">the</w>
     <w lemma="duchy" pos="n1" reg="Duchy" rend="hi" xml:id="A94390-001-a-0130">Dutchy</w>
     <w lemma="liberty" pos="n1" xml:id="A94390-001-a-0140">Liberty</w>
     <pc xml:id="A94390-001-a-0150">,</pc>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-0160">of</w>
     <w lemma="coventgarden" pos="nn1" rend="hi" xml:id="A94390-001-a-0170">Covent-garden</w>
     <pc xml:id="A94390-001-a-0180">,</pc>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-0190">and</w>
     <w lemma="st." pos="ab" xml:id="A94390-001-a-0200">St.</w>
     <w lemma="Martin" pos="nng1" reg="Martin's" rend="hi" xml:id="A94390-001-a-0210">Martins</w>
     <w lemma="in" pos="acp" xml:id="A94390-001-a-0220">in</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-0230">the</w>
     <w lemma="field" pos="n2" xml:id="A94390-001-a-0240">Fields</w>
     <pc unit="sentence" xml:id="A94390-001-a-0250">.</pc>
    </head>
    <p xml:id="A94390-e70">
     <w lemma="whereas" pos="cs" rend="decorinit" xml:id="A94390-001-a-0260">WHereas</w>
     <w lemma="I" pos="pns" xml:id="A94390-001-a-0270">I</w>
     <w lemma="have" pos="vvb" xml:id="A94390-001-a-0280">have</w>
     <w lemma="receive" pos="vvn" xml:id="A94390-001-a-0290">received</w>
     <w lemma="a" pos="d" xml:id="A94390-001-a-0300">a</w>
     <w lemma="warrant" pos="n1" xml:id="A94390-001-a-0310">Warrant</w>
     <w lemma="from" pos="acp" xml:id="A94390-001-a-0320">from</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-0330">the</w>
     <w lemma="right" pos="n1-j" xml:id="A94390-001-a-0340">Right</w>
     <w lemma="honourable" pos="j" xml:id="A94390-001-a-0350">Honourable</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-0360">the</w>
     <w lemma="earl" pos="n1" xml:id="A94390-001-a-0370">Earl</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-0380">of</w>
     <w lemma="Linsey" pos="nn1" rend="hi" xml:id="A94390-001-a-0390">Linsey</w>
     <w lemma="lord" pos="n1" xml:id="A94390-001-a-0400">Lord</w>
     <w lemma="great" pos="j" xml:id="A94390-001-a-0410">Great</w>
     <w lemma="chamberlain" pos="n1" xml:id="A94390-001-a-0420">Chamberlain</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-0430">of</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A94390-001-a-0440">England</w>
     <pc xml:id="A94390-001-a-0450">,</pc>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-0460">to</w>
     <w lemma="make" pos="vvi" xml:id="A94390-001-a-0470">make</w>
     <w lemma="proclamation" pos="n1" xml:id="A94390-001-a-0480">Proclamation</w>
     <w lemma="in" pos="acp" xml:id="A94390-001-a-0490">in</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-0500">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A94390-001-a-0510">Majesties</w>
     <w lemma="name" pos="n1" xml:id="A94390-001-a-0520">Name</w>
     <pc xml:id="A94390-001-a-0530">,</pc>
     <w lemma="as" pos="acp" xml:id="A94390-001-a-0540">as</w>
     <w lemma="follow" pos="vvz" xml:id="A94390-001-a-0550">followeth</w>
     <pc xml:id="A94390-001-a-0560">,</pc>
     <w lemma="namely" pos="av" xml:id="A94390-001-a-0570">Namely</w>
     <pc xml:id="A94390-001-a-0580">,</pc>
     <w lemma="that" pos="cs" xml:id="A94390-001-a-0590">That</w>
     <w lemma="no" pos="dx" xml:id="A94390-001-a-0600">no</w>
     <w lemma="person" pos="n2" xml:id="A94390-001-a-0610">persons</w>
     <w lemma="but" pos="acp" xml:id="A94390-001-a-0620">but</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-0630">the</w>
     <w lemma="nobility" pos="n1" xml:id="A94390-001-a-0640">Nobility</w>
     <pc xml:id="A94390-001-a-0650">,</pc>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-0660">and</w>
     <w lemma="officer" pos="n2" xml:id="A94390-001-a-0670">Officers</w>
     <w lemma="appoint" pos="vvn" xml:id="A94390-001-a-0680">appointed</w>
     <pc xml:id="A94390-001-a-0690">,</pc>
     <w lemma="come" pos="vvn" xml:id="A94390-001-a-0700">come</w>
     <w lemma="within" pos="acp" xml:id="A94390-001-a-0710">within</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-0720">the</w>
     <w lemma="rail" pos="n2" reg="Rails" xml:id="A94390-001-a-0730">Rayles</w>
     <w lemma="wherein" pos="crq" xml:id="A94390-001-a-0740">wherein</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-0750">His</w>
     <w lemma="majesty" pos="n1" xml:id="A94390-001-a-0760">Majesty</w>
     <w lemma="shall" pos="vmb" xml:id="A94390-001-a-0770">shall</w>
     <w lemma="proceed" pos="vvi" xml:id="A94390-001-a-0780">proceed</w>
     <w lemma="unto" pos="acp" xml:id="A94390-001-a-0790">unto</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-0800">His</w>
     <w lemma="royal" pos="j" xml:id="A94390-001-a-0810">Royal</w>
     <w lemma="coronation" pos="n1" xml:id="A94390-001-a-0820">Coronation</w>
     <pc xml:id="A94390-001-a-0830">,</pc>
     <w lemma="nor" pos="ccx" xml:id="A94390-001-a-0840">nor</w>
     <w lemma="after" pos="acp" xml:id="A94390-001-a-0850">after</w>
     <w lemma="on" pos="acp" xml:id="A94390-001-a-0860">on</w>
     <w lemma="that" pos="d" xml:id="A94390-001-a-0870">that</w>
     <w lemma="day" pos="n1" xml:id="A94390-001-a-0880">day</w>
     <pc xml:id="A94390-001-a-0890">,</pc>
     <w lemma="until" pos="acp" xml:id="A94390-001-a-0900">until</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-0910">His</w>
     <w lemma="majesty" pos="n1" xml:id="A94390-001-a-0920">Majesty</w>
     <w lemma="be" pos="vvi" xml:id="A94390-001-a-0930">be</w>
     <w lemma="return" pos="vvn" xml:id="A94390-001-a-0940">returned</w>
     <w lemma="into" pos="acp" xml:id="A94390-001-a-0950">into</w>
     <w lemma="Westminster-hall" pos="nn1" rend="hi" xml:id="A94390-001-a-0960">Westminster-hall</w>
     <w lemma="from" pos="acp" xml:id="A94390-001-a-0970">from</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-0980">the</w>
     <w lemma="church" pos="n1" xml:id="A94390-001-a-0990">Church</w>
     <pc unit="sentence" xml:id="A94390-001-a-1000">.</pc>
     <w lemma="nor" pos="ccx" xml:id="A94390-001-a-1010">Nor</w>
     <w lemma="that" pos="d" xml:id="A94390-001-a-1020">that</w>
     <w lemma="any" pos="d" xml:id="A94390-001-a-1030">any</w>
     <w lemma="person" pos="n2" xml:id="A94390-001-a-1040">persons</w>
     <w lemma="but" pos="acp" xml:id="A94390-001-a-1050">but</w>
     <w lemma="such" pos="d" xml:id="A94390-001-a-1060">such</w>
     <w lemma="as" pos="acp" xml:id="A94390-001-a-1070">as</w>
     <w lemma="be" pos="vvb" xml:id="A94390-001-a-1080">are</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-1090">to</w>
     <w lemma="go" pos="vvi" xml:id="A94390-001-a-1100">go</w>
     <w lemma="in" pos="acp" xml:id="A94390-001-a-1110">in</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-1120">the</w>
     <w lemma="say" pos="vvd" xml:id="A94390-001-a-1130">said</w>
     <w lemma="proceed" pos="vvg" xml:id="A94390-001-a-1140">proceeding</w>
     <pc xml:id="A94390-001-a-1150">,</pc>
     <w lemma="presume" pos="vvb" xml:id="A94390-001-a-1160">presume</w>
     <w lemma="on" pos="acp" xml:id="A94390-001-a-1170">on</w>
     <w lemma="that" pos="d" xml:id="A94390-001-a-1180">that</w>
     <w lemma="day" pos="n1" xml:id="A94390-001-a-1190">day</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-1200">to</w>
     <w lemma="come" pos="vvi" xml:id="A94390-001-a-1210">come</w>
     <w lemma="into" pos="acp" xml:id="A94390-001-a-1220">into</w>
     <w lemma="any" pos="d" xml:id="A94390-001-a-1230">any</w>
     <w lemma="room" pos="n2" xml:id="A94390-001-a-1240">Rooms</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-1250">of</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-1260">the</w>
     <w lemma="lord" pos="n2" xml:id="A94390-001-a-1270">Lords</w>
     <w lemma="house" pos="n1" xml:id="A94390-001-a-1280">House</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-1290">of</w>
     <w lemma="parliament" pos="n1" xml:id="A94390-001-a-1300">Parliament</w>
     <pc xml:id="A94390-001-a-1310">,</pc>
     <w lemma="the" pos="d" xml:id="A94390-001-a-1320">the</w>
     <w lemma="paint" pos="j-vn" xml:id="A94390-001-a-1330">Painted</w>
     <w lemma="chamber" pos="n1" xml:id="A94390-001-a-1340">Chamber</w>
     <pc xml:id="A94390-001-a-1350">,</pc>
     <w lemma="court" pos="vvz" xml:id="A94390-001-a-1360">Courts</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-1370">of</w>
     <w lemma="request" pos="n2" xml:id="A94390-001-a-1380">Requests</w>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-1390">and</w>
     <w lemma="ward" pos="n2" xml:id="A94390-001-a-1400">Wards</w>
     <pc xml:id="A94390-001-a-1410">,</pc>
     <w lemma="in" pos="acp" xml:id="A94390-001-a-1420">in</w>
     <w lemma="Westminster-hall" pos="nn1" rend="hi" xml:id="A94390-001-a-1430">Westminster-hall</w>
     <pc xml:id="A94390-001-a-1440">,</pc>
     <w lemma="until" pos="acp" xml:id="A94390-001-a-1450">until</w>
     <w lemma="such" pos="d" xml:id="A94390-001-a-1460">such</w>
     <w lemma="time" pos="n1" xml:id="A94390-001-a-1470">time</w>
     <w lemma="as" pos="acp" xml:id="A94390-001-a-1480">as</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-1490">His</w>
     <w lemma="majesty" pos="n1" xml:id="A94390-001-a-1500">Majesty</w>
     <w lemma="be" pos="vvi" xml:id="A94390-001-a-1510">be</w>
     <w lemma="go" pos="vvn" xml:id="A94390-001-a-1520">gone</w>
     <w lemma="thence" pos="av" xml:id="A94390-001-a-1530">thence</w>
     <w lemma="to" pos="acp" xml:id="A94390-001-a-1540">to</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-1550">the</w>
     <w lemma="church" pos="n1" xml:id="A94390-001-a-1560">Church</w>
     <pc xml:id="A94390-001-a-1570">,</pc>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-1580">and</w>
     <w lemma="then" pos="av" xml:id="A94390-001-a-1590">then</w>
     <w lemma="only" pos="av-j" reg="only" xml:id="A94390-001-a-1600">onely</w>
     <w lemma="such" pos="d" xml:id="A94390-001-a-1610">such</w>
     <w lemma="as" pos="acp" xml:id="A94390-001-a-1620">as</w>
     <w lemma="be" pos="vvb" xml:id="A94390-001-a-1630">are</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-1640">to</w>
     <w lemma="do" pos="vvi" xml:id="A94390-001-a-1650">do</w>
     <w lemma="duty" pos="n1" xml:id="A94390-001-a-1660">duty</w>
     <w lemma="in" pos="acp" xml:id="A94390-001-a-1670">in</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-1680">the</w>
     <w lemma="say" pos="j-vn" xml:id="A94390-001-a-1690">said</w>
     <w lemma="hall" pos="n1" xml:id="A94390-001-a-1700">Hall</w>
     <w lemma="at" pos="acp" xml:id="A94390-001-a-1710">at</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-1720">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A94390-001-a-1730">Majesties</w>
     <w lemma="return" pos="n1" xml:id="A94390-001-a-1740">return</w>
     <w lemma="to" pos="acp" xml:id="A94390-001-a-1750">to</w>
     <w lemma="dinner" pos="n1" xml:id="A94390-001-a-1760">Dinner</w>
     <pc unit="sentence" xml:id="A94390-001-a-1770">.</pc>
     <w lemma="as" pos="acp" xml:id="A94390-001-a-1780">As</w>
     <w lemma="also" pos="av" xml:id="A94390-001-a-1790">also</w>
     <w lemma="that" pos="cs" xml:id="A94390-001-a-1800">that</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-1810">the</w>
     <w lemma="say" pos="vvd" xml:id="A94390-001-a-1820">said</w>
     <w lemma="constable" pos="n2" xml:id="A94390-001-a-1830">Constables</w>
     <w lemma="be" pos="vvb" xml:id="A94390-001-a-1840">be</w>
     <w lemma="careful" pos="j" xml:id="A94390-001-a-1850">careful</w>
     <w lemma="that" pos="cs" xml:id="A94390-001-a-1860">that</w>
     <w lemma="no" pos="dx" xml:id="A94390-001-a-1870">no</w>
     <w lemma="person" pos="n2" xml:id="A94390-001-a-1880">persons</w>
     <w lemma="whatever" pos="crq" xml:id="A94390-001-a-1890">whatever</w>
     <w lemma="presume" pos="vvb" xml:id="A94390-001-a-1900">presume</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-1910">to</w>
     <w lemma="stand" pos="vvi" xml:id="A94390-001-a-1920">stand</w>
     <w lemma="within" pos="acp" xml:id="A94390-001-a-1930">within</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-1940">the</w>
     <w lemma="rail" pos="n2" reg="Rails" xml:id="A94390-001-a-1950">Rayles</w>
     <w lemma="that" pos="cs" xml:id="A94390-001-a-1960">that</w>
     <w lemma="shall" pos="vmb" xml:id="A94390-001-a-1970">shall</w>
     <w lemma="be" pos="vvi" xml:id="A94390-001-a-1980">be</w>
     <w lemma="set" pos="vvn" xml:id="A94390-001-a-1990">set</w>
     <w lemma="up" pos="acp" xml:id="A94390-001-a-2000">up</w>
     <w lemma="from" pos="acp" xml:id="A94390-001-a-2010">from</w>
     <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A94390-001-a-2020">Whitehall</w>
     <w lemma="to" pos="acp" xml:id="A94390-001-a-2030">to</w>
     <w lemma="Temple-bar" pos="nn1" rend="hi" xml:id="A94390-001-a-2040">Temple-bar</w>
     <pc xml:id="A94390-001-a-2050">,</pc>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-2060">to</w>
     <w lemma="hinder" pos="vvi" xml:id="A94390-001-a-2070">hinder</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-2080">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A94390-001-a-2090">Majesties</w>
     <w lemma="proceed" pos="vvg" xml:id="A94390-001-a-2100">proceeding</w>
     <w lemma="on" pos="acp" xml:id="A94390-001-a-2110">on</w>
     <w lemma="that" pos="d" xml:id="A94390-001-a-2120">that</w>
     <w lemma="day" pos="n1" xml:id="A94390-001-a-2130">day</w>
     <w lemma="when" pos="crq" xml:id="A94390-001-a-2140">when</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-2150">His</w>
     <w lemma="majesty" pos="n1" xml:id="A94390-001-a-2160">Majesty</w>
     <w lemma="shall" pos="vmb" xml:id="A94390-001-a-2170">shall</w>
     <w lemma="ride" pos="vvi" xml:id="A94390-001-a-2180">ride</w>
     <w lemma="from" pos="acp" xml:id="A94390-001-a-2190">from</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-2200">the</w>
     <w lemma="tower" pos="n1" rend="hi" xml:id="A94390-001-a-2210">Tower</w>
     <w lemma="unto" pos="acp" xml:id="A94390-001-a-2220">unto</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-2230">His</w>
     <w lemma="palace" pos="n1" xml:id="A94390-001-a-2240">Palace</w>
     <w lemma="at" pos="acp" xml:id="A94390-001-a-2250">at</w>
     <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A94390-001-a-2260">Whitehall</w>
     <pc xml:id="A94390-001-a-2270">,</pc>
     <w lemma="until" pos="acp" xml:id="A94390-001-a-2280">until</w>
     <w lemma="he" pos="pns" xml:id="A94390-001-a-2290">he</w>
     <w lemma="shall" pos="vmb" xml:id="A94390-001-a-2300">shall</w>
     <w lemma="be" pos="vvi" xml:id="A94390-001-a-2310">be</w>
     <w lemma="arrive" pos="vvn" xml:id="A94390-001-a-2320">arrived</w>
     <w lemma="there" pos="av" xml:id="A94390-001-a-2330">there</w>
     <pc xml:id="A94390-001-a-2340">;</pc>
     <w lemma="but" pos="acp" xml:id="A94390-001-a-2350">but</w>
     <w lemma="that" pos="cs" xml:id="A94390-001-a-2360">that</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-2370">the</w>
     <w lemma="say" pos="vvd" xml:id="A94390-001-a-2380">said</w>
     <w lemma="rail" pos="n2" reg="Rails" xml:id="A94390-001-a-2390">Rayles</w>
     <w lemma="shall" pos="vmb" xml:id="A94390-001-a-2400">shall</w>
     <w lemma="be" pos="vvi" xml:id="A94390-001-a-2410">be</w>
     <w lemma="keep" pos="vvn" xml:id="A94390-001-a-2420">kept</w>
     <w lemma="clear" pos="j" xml:id="A94390-001-a-2430">clear</w>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-2440">and</w>
     <w lemma="void" pos="j" xml:id="A94390-001-a-2450">void</w>
     <w lemma="all" pos="d" xml:id="A94390-001-a-2460">all</w>
     <w lemma="that" pos="d" xml:id="A94390-001-a-2470">that</w>
     <w lemma="day" pos="n1" xml:id="A94390-001-a-2480">day</w>
     <w lemma="for" pos="acp" xml:id="A94390-001-a-2490">for</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-2500">the</w>
     <w lemma="free" pos="j" xml:id="A94390-001-a-2510">free</w>
     <w lemma="passage" pos="n1" xml:id="A94390-001-a-2520">passage</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-2530">of</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-2540">His</w>
     <w lemma="majesty" pos="n1" xml:id="A94390-001-a-2550">Majesty</w>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-2560">and</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-2570">of</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-2580">His</w>
     <w lemma="officer" pos="n2" xml:id="A94390-001-a-2590">Officers</w>
     <w lemma="that" pos="cs" xml:id="A94390-001-a-2600">that</w>
     <w lemma="be" pos="vvb" xml:id="A94390-001-a-2610">are</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-2620">to</w>
     <w lemma="attend" pos="vvi" xml:id="A94390-001-a-2630">attend</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-2640">the</w>
     <w lemma="ceremony" pos="n1" xml:id="A94390-001-a-2650">Ceremony</w>
     <pc xml:id="A94390-001-a-2660">:</pc>
     <w lemma="these" pos="d" xml:id="A94390-001-a-2670">These</w>
     <w lemma="be" pos="vvb" xml:id="A94390-001-a-2680">are</w>
     <w lemma="therefore" pos="av" xml:id="A94390-001-a-2690">therefore</w>
     <w lemma="in" pos="acp" xml:id="A94390-001-a-2700">in</w>
     <w lemma="his" pos="po" xml:id="A94390-001-a-2710">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A94390-001-a-2720">Majesties</w>
     <w lemma="name" pos="n1" xml:id="A94390-001-a-2730">Name</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-2740">to</w>
     <w lemma="require" pos="vvi" xml:id="A94390-001-a-2750">require</w>
     <w lemma="you" pos="pn" xml:id="A94390-001-a-2760">you</w>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-2770">and</w>
     <w lemma="every" pos="d" xml:id="A94390-001-a-2780">every</w>
     <w lemma="one" pos="pi" xml:id="A94390-001-a-2790">one</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-2800">of</w>
     <w lemma="you" pos="pn" xml:id="A94390-001-a-2810">you</w>
     <pc xml:id="A94390-001-a-2820">,</pc>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-2830">to</w>
     <w lemma="cause" pos="vvi" xml:id="A94390-001-a-2840">cause</w>
     <w lemma="this" pos="d" xml:id="A94390-001-a-2850">this</w>
     <w lemma="proclamation" pos="n1" xml:id="A94390-001-a-2860">Proclamation</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-2870">to</w>
     <w lemma="be" pos="vvi" xml:id="A94390-001-a-2880">be</w>
     <w lemma="read" pos="vvn" xml:id="A94390-001-a-2890">read</w>
     <w lemma="in" pos="acp" xml:id="A94390-001-a-2900">in</w>
     <w lemma="your" pos="po" xml:id="A94390-001-a-2910">your</w>
     <w lemma="respective" pos="j" xml:id="A94390-001-a-2920">respective</w>
     <w lemma="church" pos="n2" xml:id="A94390-001-a-2930">Churches</w>
     <pc xml:id="A94390-001-a-2940">,</pc>
     <w lemma="the" pos="d" xml:id="A94390-001-a-2950">the</w>
     <w lemma="Sunday" pos="nn1" xml:id="A94390-001-a-2960">Sunday</w>
     <w lemma="before" pos="acp" xml:id="A94390-001-a-2970">before</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-2980">the</w>
     <w lemma="solemnisation" pos="n1" reg="Solemnisation" xml:id="A94390-001-a-2990">Solemnization</w>
     <w lemma="of" pos="acp" xml:id="A94390-001-a-3000">of</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-3010">the</w>
     <w lemma="coronation" pos="n1" xml:id="A94390-001-a-3020">Coronation</w>
     <pc xml:id="A94390-001-a-3030">,</pc>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-3040">and</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-3050">to</w>
     <w lemma="affix" pos="vvi" xml:id="A94390-001-a-3060">affix</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-3070">the</w>
     <w lemma="same" pos="d" xml:id="A94390-001-a-3080">same</w>
     <w lemma="in" pos="acp" xml:id="A94390-001-a-3090">in</w>
     <w lemma="place" pos="n2" xml:id="A94390-001-a-3100">places</w>
     <w lemma="convenient" pos="j" xml:id="A94390-001-a-3110">convenient</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-3120">to</w>
     <w lemma="be" pos="vvi" xml:id="A94390-001-a-3130">be</w>
     <w lemma="see" pos="vvn" xml:id="A94390-001-a-3140">seen</w>
     <pc xml:id="A94390-001-a-3150">;</pc>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-3160">and</w>
     <w lemma="to" pos="prt" xml:id="A94390-001-a-3170">to</w>
     <w lemma="be" pos="vvi" xml:id="A94390-001-a-3180">be</w>
     <w lemma="careful" pos="j" xml:id="A94390-001-a-3190">careful</w>
     <w lemma="in" pos="acp" xml:id="A94390-001-a-3200">in</w>
     <w lemma="your" pos="po" xml:id="A94390-001-a-3210">your</w>
     <w lemma="several" pos="j" xml:id="A94390-001-a-3220">several</w>
     <w lemma="station" pos="n2" xml:id="A94390-001-a-3230">stations</w>
     <w lemma="that" pos="cs" xml:id="A94390-001-a-3240">that</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-3250">the</w>
     <w lemma="content" pos="n2" xml:id="A94390-001-a-3260">contents</w>
     <w lemma="hereof" pos="av" xml:id="A94390-001-a-3270">hereof</w>
     <w lemma="be" pos="vvi" xml:id="A94390-001-a-3280">be</w>
     <w lemma="due" pos="av-j" xml:id="A94390-001-a-3290">duly</w>
     <w lemma="observe" pos="vvn" xml:id="A94390-001-a-3300">observed</w>
     <pc unit="sentence" xml:id="A94390-001-a-3310">.</pc>
    </p>
    <closer xml:id="A94390-e160">
     <dateline xml:id="A94390-e170">
      <w lemma="give" pos="vvn" xml:id="A94390-001-a-3320">Given</w>
      <w lemma="at" pos="acp" xml:id="A94390-001-a-3330">at</w>
      <w lemma="Whitehall" pos="nn1" rend="hi" xml:id="A94390-001-a-3340">Whitehall</w>
      <pc xml:id="A94390-001-a-3350">,</pc>
      <w lemma="by" pos="acp" xml:id="A94390-001-a-3360">by</w>
      <w lemma="the" pos="d" xml:id="A94390-001-a-3370">the</w>
      <w lemma="authority" pos="n1" xml:id="A94390-001-a-3380">Authority</w>
      <w lemma="above" pos="acp" xml:id="A94390-001-a-3390">above</w>
      <w lemma="name" pos="vvn" xml:id="A94390-001-a-3400">named</w>
      <pc xml:id="A94390-001-a-3410">,</pc>
      <date xml:id="A94390-e190">
       <w lemma="the" pos="d" xml:id="A94390-001-a-3420">the</w>
       <w lemma="eight" pos="crd" xml:id="A94390-001-a-3430">Eight</w>
       <w lemma="day" pos="n1" xml:id="A94390-001-a-3440">day</w>
       <w lemma="of" pos="acp" xml:id="A94390-001-a-3450">of</w>
       <w lemma="April" pos="nn1" rend="hi" xml:id="A94390-001-a-3460">April</w>
       <pc xml:id="A94390-001-a-3470">,</pc>
       <w lemma="one" pos="crd" xml:id="A94390-001-a-3480">One</w>
       <w lemma="thousand" pos="crd" xml:id="A94390-001-a-3490">thousand</w>
       <w lemma="six" pos="crd" xml:id="A94390-001-a-3500">six</w>
       <w lemma="hundred" pos="crd" xml:id="A94390-001-a-3510">hundred</w>
       <w lemma="sixty" pos="crd" xml:id="A94390-001-a-3520">sixty</w>
       <w lemma="one" pos="crd" xml:id="A94390-001-a-3530">one</w>
      </date>
     </dateline>
     <pc xml:id="A94390-001-a-3540">,</pc>
     <w lemma="and" pos="cc" xml:id="A94390-001-a-3550">and</w>
     <signed xml:id="A94390-e210">
      <w lemma="sign" pos="vvn" xml:id="A94390-001-a-3560">signed</w>
      <w lemma="by" pos="acp" xml:id="A94390-001-a-3570">by</w>
      <w lemma="i" pos="pno" xml:id="A94390-001-a-3580">me</w>
      <w lemma="his" pos="po" xml:id="A94390-001-a-3590">His</w>
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A94390-001-a-3600">Majesties</w>
      <w lemma="knight" pos="n1" xml:id="A94390-001-a-3610">Knight</w>
      <w lemma="Martial" pos="nn1" reg="Martial" xml:id="A94390-001-a-3620">Marshall</w>
      <pc xml:id="A94390-001-a-3630">,</pc>
      <w lemma="William" pos="nn1" xml:id="A94390-001-a-3640">William</w>
      <w lemma="throckmorton" pos="nn1" xml:id="A94390-001-a-3650">Throckmorton</w>
      <pc unit="sentence" xml:id="A94390-001-a-3660">.</pc>
     </signed>
    </closer>
    <closer xml:id="A94390-e220">
     <w lemma="GOD" pos="nn1" xml:id="A94390-001-a-3670">GOD</w>
     <w lemma="save" pos="acp" xml:id="A94390-001-a-3680">SAVE</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-3690">THE</w>
     <w lemma="king" pos="n1" xml:id="A94390-001-a-3700">KING</w>
     <pc unit="sentence" xml:id="A94390-001-a-3710">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A94390-e230">
   <div type="colophon" xml:id="A94390-e240">
    <p xml:id="A94390-e250">
     <w lemma="LONDON" pos="nn1" rend="hi" xml:id="A94390-001-a-3720">LONDON</w>
     <pc xml:id="A94390-001-a-3730">,</pc>
     <w lemma="print" pos="vvn" xml:id="A94390-001-a-3740">Printed</w>
     <w lemma="by" pos="acp" xml:id="A94390-001-a-3750">by</w>
     <hi xml:id="A94390-e270">
      <w lemma="John" pos="nn1" xml:id="A94390-001-a-3760">John</w>
      <w lemma="bill" pos="n1" xml:id="A94390-001-a-3770">Bill</w>
     </hi>
     <pc rend="follows-hi" xml:id="A94390-001-a-3780">,</pc>
     <w lemma="printer" pos="n1" xml:id="A94390-001-a-3790">Printer</w>
     <w lemma="to" pos="acp" xml:id="A94390-001-a-3800">to</w>
     <w lemma="the" pos="d" xml:id="A94390-001-a-3810">the</w>
     <w lemma="2" pos="crd" xml:id="A94390-001-a-3820">KING's</w>
     <w lemma="most" pos="avs-d" xml:id="A94390-001-a-3830">most</w>
     <w lemma="excellent" pos="j" xml:id="A94390-001-a-3840">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="A94390-001-a-3850">MAJESTY</w>
     <pc xml:id="A94390-001-a-3860">,</pc>
     <w lemma="1661." pos="crd" xml:id="A94390-001-a-3870">1661.</w>
     <pc unit="sentence" xml:id="A94390-001-a-3880"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
