<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A94450">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the honourable Citie of London. The humble petition of Philip Skippon, Esq;</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A94450 of text R211607 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.24[7]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A94450</idno>
    <idno type="STC">Wing T1408A</idno>
    <idno type="STC">Thomason 669.f.24[7]</idno>
    <idno type="STC">ESTC R211607</idno>
    <idno type="EEBO-CITATION">99870319</idno>
    <idno type="PROQUEST">99870319</idno>
    <idno type="VID">163738</idno>
    <idno type="PROQUESTGOID">2240954245</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A94450)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163738)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f24[7])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the honourable Citie of London. The humble petition of Philip Skippon, Esq;</title>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for William Waterson,</publisher>
      <pubPlace>London :</pubPlace>
      <date>[1660]</date>
     </publicationStmt>
     <notesStmt>
      <note>A satire, not in fact by Philip Skippon. -- Cf. Wing.</note>
      <note>Publication date from Wing.</note>
      <note>Annotation on Thomason copy: "March. 8. 1659".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Skippon, Philip, d. 1660 -- Humor -- Early works to 1800.</term>
     <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660 -- Humor -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A94450</ep:tcp>
    <ep:estc> R211607</ep:estc>
    <ep:stc> (Thomason 669.f.24[7]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>To the honourable citie of London· The humble petition of Philip Skippon, Esq;</ep:title>
    <ep:author>anon.</ep:author>
    <ep:publicationYear>1660</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>473</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-07</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2007-08</date>
    <label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2007-09</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2007-09</date>
    <label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A94450-e10">
  <body xml:id="A94450-e20">
   <pb facs="tcp:163738:1" rend="simple:additions" xml:id="A94450-001-a"/>
   <div type="text" xml:id="A94450-e30">
    <head xml:id="A94450-e40">
     <w lemma="to" pos="acp" xml:id="A94450-001-a-0010">TO</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-0020">THE</w>
     <w lemma="honourable" pos="j" xml:id="A94450-001-a-0030">HONOURABLE</w>
     <w lemma="city" pos="n1" reg="city" xml:id="A94450-001-a-0040">CITIE</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-0050">OF</w>
     <w lemma="LONDON" pos="nn1" xml:id="A94450-001-a-0060">LONDON</w>
     <pc unit="sentence" xml:id="A94450-001-a-0070">.</pc>
    </head>
    <head xml:id="A94450-e50">
     <hi xml:id="A94450-e60">
      <w lemma="the" pos="d" reg="tbe" xml:id="A94450-001-a-0080">The</w>
      <w lemma="humble" pos="j" xml:id="A94450-001-a-0090">Humble</w>
      <w lemma="petition" pos="n1" xml:id="A94450-001-a-0100">Petition</w>
      <w lemma="of" pos="acp" xml:id="A94450-001-a-0110">of</w>
     </hi>
     <w lemma="Philip" pos="nn1" xml:id="A94450-001-a-0120">Philip</w>
     <w lemma="Skippon" pos="nn1" xml:id="A94450-001-a-0130">Skippon</w>
     <pc xml:id="A94450-001-a-0140">,</pc>
     <hi xml:id="A94450-e70">
      <w lemma="n/a" pos="fla" xml:id="A94450-001-a-0150">Efque</w>
      <pc unit="sentence" xml:id="A94450-001-a-0160"/>
     </hi>
    </head>
    <opener xml:id="A94450-e80">
     <w lemma="show" pos="vvz" reg="Showeth" xml:id="A94450-001-a-0170">SHEWETH</w>
     <pc xml:id="A94450-001-a-0180">,</pc>
    </opener>
    <p xml:id="A94450-e90">
     <w lemma="that" pos="cs" xml:id="A94450-001-a-0190">THat</w>
     <w lemma="your" pos="po" xml:id="A94450-001-a-0200">your</w>
     <w lemma="petitioner" pos="n1" xml:id="A94450-001-a-0210">Petitioner</w>
     <w lemma="be" pos="vvd" xml:id="A94450-001-a-0220">was</w>
     <w lemma="a" pos="d" xml:id="A94450-001-a-0230">a</w>
     <w lemma="very" pos="j" xml:id="A94450-001-a-0240">very</w>
     <w lemma="eminent" pos="j" xml:id="A94450-001-a-0250">eminent</w>
     <w lemma="instrument" pos="n1" xml:id="A94450-001-a-0260">Instrument</w>
     <w lemma="in" pos="acp" xml:id="A94450-001-a-0270">in</w>
     <w lemma="carry" pos="vvg" xml:id="A94450-001-a-0280">carrying</w>
     <w lemma="on" pos="acp" xml:id="A94450-001-a-0290">on</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-0300">the</w>
     <w lemma="work" pos="n1" xml:id="A94450-001-a-0310">Work</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-0320">of</w>
     <w lemma="reformation" pos="n1" xml:id="A94450-001-a-0330">Reformation</w>
     <pc xml:id="A94450-001-a-0340">;</pc>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-0350">and</w>
     <w lemma="that" pos="cs" xml:id="A94450-001-a-0360">that</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-0370">he</w>
     <w lemma="do" pos="vvd" xml:id="A94450-001-a-0380">did</w>
     <w lemma="promote" pos="vvi" xml:id="A94450-001-a-0390">promote</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-0400">the</w>
     <w lemma="good" pos="j" xml:id="A94450-001-a-0410">Good</w>
     <w lemma="cause" pos="n1" xml:id="A94450-001-a-0420">Cause</w>
     <w lemma="with" pos="acp" xml:id="A94450-001-a-0430">with</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-0440">the</w>
     <w lemma="same" pos="d" xml:id="A94450-001-a-0450">same</w>
     <w lemma="zeal" pos="n1" xml:id="A94450-001-a-0460">zeal</w>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-0470">and</w>
     <w lemma="endeavour" pos="n2" xml:id="A94450-001-a-0480">endeavours</w>
     <w lemma="as" pos="acp" xml:id="A94450-001-a-0490">as</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-0500">he</w>
     <w lemma="do" pos="vvd" xml:id="A94450-001-a-0510">did</w>
     <w lemma="advance" pos="vvi" xml:id="A94450-001-a-0520">advance</w>
     <w lemma="himself" pos="pr" xml:id="A94450-001-a-0530">himself</w>
     <pc xml:id="A94450-001-a-0540">;</pc>
     <w lemma="whereto" pos="crq" xml:id="A94450-001-a-0550">whereto</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-0560">he</w>
     <w lemma="be" pos="vvz" xml:id="A94450-001-a-0570">is</w>
     <w lemma="in" pos="acp" xml:id="A94450-001-a-0580">in</w>
     <w lemma="gratitude" pos="n1" xml:id="A94450-001-a-0590">gratitude</w>
     <w lemma="oblige" pos="vvn" xml:id="A94450-001-a-0600">obliged</w>
     <w lemma="to" pos="prt" xml:id="A94450-001-a-0610">to</w>
     <w lemma="say" pos="vvi" xml:id="A94450-001-a-0620">say</w>
     <pc xml:id="A94450-001-a-0630">,</pc>
     <w lemma="you" pos="pn" xml:id="A94450-001-a-0640">you</w>
     <w lemma="be" pos="vvd" xml:id="A94450-001-a-0650">were</w>
     <pc join="right" xml:id="A94450-001-a-0660">(</pc>
     <w lemma="if" pos="cs" xml:id="A94450-001-a-0670">if</w>
     <w lemma="not" pos="xx" xml:id="A94450-001-a-0680">not</w>
     <w lemma="more" pos="avc-d" xml:id="A94450-001-a-0690">more</w>
     <w lemma="forward" pos="av-j" xml:id="A94450-001-a-0700">forward</w>
     <pc xml:id="A94450-001-a-0710">)</pc>
     <w lemma="yet" pos="av" xml:id="A94450-001-a-0720">yet</w>
     <w lemma="equal" pos="av-j" xml:id="A94450-001-a-0730">equally</w>
     <w lemma="prompt" pos="vvi" xml:id="A94450-001-a-0740">prompt</w>
     <w lemma="with" pos="acp" xml:id="A94450-001-a-0750">with</w>
     <w lemma="your" pos="po" xml:id="A94450-001-a-0760">your</w>
     <w lemma="petitioner" pos="n1" xml:id="A94450-001-a-0770">Petitioner</w>
     <pc unit="sentence" xml:id="A94450-001-a-0780">.</pc>
    </p>
    <p xml:id="A94450-e100">
     <w lemma="but" pos="acp" xml:id="A94450-001-a-0790">But</w>
     <w lemma="now" pos="av" xml:id="A94450-001-a-0800">now</w>
     <w lemma="so" pos="av" xml:id="A94450-001-a-0810">so</w>
     <w lemma="it" pos="pn" xml:id="A94450-001-a-0820">it</w>
     <w lemma="be" pos="vvz" xml:id="A94450-001-a-0830">is</w>
     <pc join="right" xml:id="A94450-001-a-0840">(</pc>
     <w lemma="may" pos="vmb" xml:id="A94450-001-a-0850">may</w>
     <w lemma="it" pos="pn" xml:id="A94450-001-a-0860">it</w>
     <w lemma="please" pos="vvi" xml:id="A94450-001-a-0870">please</w>
     <w lemma="your" pos="po" xml:id="A94450-001-a-0880">your</w>
     <w lemma="honour" pos="n2" xml:id="A94450-001-a-0890">Honours</w>
     <pc xml:id="A94450-001-a-0900">)</pc>
     <w lemma="that" pos="cs" xml:id="A94450-001-a-0910">that</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-0920">he</w>
     <w lemma="find" pos="vvz" xml:id="A94450-001-a-0930">finds</w>
     <w lemma="all" pos="d" xml:id="A94450-001-a-0940">all</w>
     <w lemma="that" pos="d" xml:id="A94450-001-a-0950">that</w>
     <w lemma="former" pos="j" xml:id="A94450-001-a-0960">former</w>
     <w lemma="affection" pos="n1" xml:id="A94450-001-a-0970">affection</w>
     <w lemma="towards" pos="acp" xml:id="A94450-001-a-0980">towards</w>
     <w lemma="he" pos="pno" xml:id="A94450-001-a-0990">him</w>
     <w lemma="so" pos="av" xml:id="A94450-001-a-1000">so</w>
     <w lemma="cold" pos="j" xml:id="A94450-001-a-1010">cold</w>
     <w lemma="since" pos="acp" xml:id="A94450-001-a-1020">since</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-1030">his</w>
     <w lemma="patron" pos="n1" xml:id="A94450-001-a-1040">Patron</w>
     <w lemma="Oliver" pos="nn1" rend="hi" xml:id="A94450-001-a-1050">Oliver</w>
     <w lemma="forsake" pos="vvd" xml:id="A94450-001-a-1060">forsook</w>
     <w lemma="this" pos="d" xml:id="A94450-001-a-1070">this</w>
     <w lemma="light" pos="n1" xml:id="A94450-001-a-1080">light</w>
     <pc xml:id="A94450-001-a-1090">,</pc>
     <w lemma="that" pos="cs" xml:id="A94450-001-a-1100">that</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-1110">he</w>
     <w lemma="fear" pos="vvz" xml:id="A94450-001-a-1120">fears</w>
     <w lemma="you" pos="pn" xml:id="A94450-001-a-1130">you</w>
     <w lemma="will" pos="vmb" xml:id="A94450-001-a-1140">will</w>
     <w lemma="bring" pos="vvi" xml:id="A94450-001-a-1150">bring</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-1160">his</w>
     <w lemma="grey" pos="j" reg="grey" xml:id="A94450-001-a-1170">gray</w>
     <w lemma="hair" pos="n2" reg="hairs" xml:id="A94450-001-a-1180">haires</w>
     <w lemma="down" pos="acp" xml:id="A94450-001-a-1190">down</w>
     <w lemma="with" pos="acp" xml:id="A94450-001-a-1200">with</w>
     <w lemma="sorrow" pos="n1" xml:id="A94450-001-a-1210">sorrow</w>
     <w lemma="to" pos="acp" xml:id="A94450-001-a-1220">to</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-1230">his</w>
     <w lemma="grave" pos="n1" xml:id="A94450-001-a-1240">grave</w>
     <pc unit="sentence" xml:id="A94450-001-a-1250">.</pc>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-1260">He</w>
     <w lemma="will" pos="vmd" xml:id="A94450-001-a-1270">would</w>
     <w lemma="attribute" pos="vvi" xml:id="A94450-001-a-1280">attribute</w>
     <w lemma="this" pos="d" xml:id="A94450-001-a-1290">this</w>
     <w lemma="change" pos="n1" xml:id="A94450-001-a-1300">change</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-1310">of</w>
     <w lemma="your" pos="po" xml:id="A94450-001-a-1320">your</w>
     <w lemma="favourable" pos="j" xml:id="A94450-001-a-1330">favourable</w>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-1340">and</w>
     <w lemma="benign" pos="j" reg="benign" xml:id="A94450-001-a-1350">benigne</w>
     <w lemma="aspect" pos="n1" xml:id="A94450-001-a-1360">Aspect</w>
     <w lemma="to" pos="acp" xml:id="A94450-001-a-1370">to</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-1380">the</w>
     <w lemma="various" pos="j" xml:id="A94450-001-a-1390">various</w>
     <w lemma="mutation" pos="n2" xml:id="A94450-001-a-1400">mutations</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-1410">of</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-1420">the</w>
     <w lemma="time" pos="n2" xml:id="A94450-001-a-1430">Times</w>
     <pc xml:id="A94450-001-a-1440">,</pc>
     <w lemma="but" pos="acp" xml:id="A94450-001-a-1450">but</w>
     <w lemma="that" pos="cs" xml:id="A94450-001-a-1460">that</w>
     <w lemma="they" pos="pns" xml:id="A94450-001-a-1470">they</w>
     <w lemma="look" pos="vvb" xml:id="A94450-001-a-1480">look</w>
     <w lemma="so" pos="av" xml:id="A94450-001-a-1490">so</w>
     <w lemma="inauspicious" pos="av-j" xml:id="A94450-001-a-1500">inauspiciously</w>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-1510">and</w>
     <w lemma="dangerous" pos="av-j" xml:id="A94450-001-a-1520">dangerously</w>
     <w lemma="on" pos="acp" xml:id="A94450-001-a-1530">on</w>
     <w lemma="he" pos="pno" xml:id="A94450-001-a-1540">him</w>
     <pc xml:id="A94450-001-a-1550">,</pc>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-1560">he</w>
     <w lemma="dare" pos="vvz" xml:id="A94450-001-a-1570">dares</w>
     <w lemma="not" pos="xx" xml:id="A94450-001-a-1580">not</w>
     <w lemma="think" pos="vvi" xml:id="A94450-001-a-1590">think</w>
     <w lemma="on" pos="acp" xml:id="A94450-001-a-1600">on</w>
     <w lemma="they" pos="pno" xml:id="A94450-001-a-1610">them</w>
     <w lemma="for" pos="acp" xml:id="A94450-001-a-1620">for</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-1630">his</w>
     <w lemma="life" pos="n1" xml:id="A94450-001-a-1640">life</w>
     <pc xml:id="A94450-001-a-1650">,</pc>
     <w lemma="although" pos="cs" xml:id="A94450-001-a-1660">although</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-1670">the</w>
     <w lemma="pleasure" pos="n1" xml:id="A94450-001-a-1680">pleasure</w>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-1690">and</w>
     <w lemma="delight" pos="n1" xml:id="A94450-001-a-1700">delight</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-1710">of</w>
     <w lemma="that" pos="d" xml:id="A94450-001-a-1720">that</w>
     <w lemma="be" pos="vvz" xml:id="A94450-001-a-1730">is</w>
     <w lemma="already" pos="av" xml:id="A94450-001-a-1740">already</w>
     <w lemma="flee" pos="vvn" xml:id="A94450-001-a-1750">fled</w>
     <w lemma="with" pos="acp" xml:id="A94450-001-a-1760">with</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-1770">his</w>
     <w lemma="money" pos="n1" xml:id="A94450-001-a-1780">money</w>
     <w lemma="beyond" pos="acp" xml:id="A94450-001-a-1790">beyond</w>
     <w lemma="sea" pos="n1" xml:id="A94450-001-a-1800">Sea</w>
     <pc xml:id="A94450-001-a-1810">,</pc>
     <w lemma="from" pos="acp" xml:id="A94450-001-a-1820">from</w>
     <w lemma="whence" pos="crq" xml:id="A94450-001-a-1830">whence</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-1840">he</w>
     <w lemma="be" pos="vvz" xml:id="A94450-001-a-1850">is</w>
     <w lemma="alarm" pos="vvn" xml:id="A94450-001-a-1860">alarum'd</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-1870">the</w>
     <w lemma="dutch" pos="jnn" rend="hi" xml:id="A94450-001-a-1880">Dutch</w>
     <w lemma="will" pos="vmb" xml:id="A94450-001-a-1890">will</w>
     <w lemma="not" pos="xx" xml:id="A94450-001-a-1900">not</w>
     <w lemma="part" pos="vvi" xml:id="A94450-001-a-1910">part</w>
     <w lemma="with" pos="acp" xml:id="A94450-001-a-1920">with</w>
     <w lemma="one" pos="crd" xml:id="A94450-001-a-1930">one</w>
     <w lemma="stiver" pos="n1" xml:id="A94450-001-a-1940">stiver</w>
     <w lemma="since" pos="acp" xml:id="A94450-001-a-1950">since</w>
     <w lemma="they" pos="pns" xml:id="A94450-001-a-1960">they</w>
     <w lemma="hear" pos="vvd" xml:id="A94450-001-a-1970">heard</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-1980">of</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-1990">his</w>
     <w lemma="disgrace" pos="n1" xml:id="A94450-001-a-2000">disgrace</w>
     <pc unit="sentence" xml:id="A94450-001-a-2010">.</pc>
    </p>
    <p xml:id="A94450-e130">
     <w lemma="in" pos="acp" xml:id="A94450-001-a-2020">In</w>
     <w lemma="this" pos="d" xml:id="A94450-001-a-2030">this</w>
     <w lemma="straight" pos="j" reg="straight" xml:id="A94450-001-a-2040">streight</w>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-2050">and</w>
     <w lemma="exigence" pos="n1" xml:id="A94450-001-a-2060">exigence</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-2070">he</w>
     <w lemma="have" pos="vvz" xml:id="A94450-001-a-2080">hath</w>
     <w lemma="betake" pos="vvn" xml:id="A94450-001-a-2090">betook</w>
     <w lemma="himself" pos="pr" xml:id="A94450-001-a-2100">himself</w>
     <w lemma="to" pos="acp" xml:id="A94450-001-a-2110">to</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-2120">his</w>
     <w lemma="book" pos="n1" xml:id="A94450-001-a-2130">Book</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-2140">of</w>
     <w lemma="corollary" pos="n2" xml:id="A94450-001-a-2150">Corollaries</w>
     <pc xml:id="A94450-001-a-2160">,</pc>
     <w lemma="the" pos="d" xml:id="A94450-001-a-2170">the</w>
     <w lemma="christian" pos="jnn" xml:id="A94450-001-a-2180">Christian</w>
     <w lemma="soldier" pos="n2" reg="soldiers" xml:id="A94450-001-a-2190">Souldiers</w>
     <w lemma="exercise" pos="n1" xml:id="A94450-001-a-2200">Exercise</w>
     <pc xml:id="A94450-001-a-2210">,</pc>
     <w lemma="but" pos="acp" xml:id="A94450-001-a-2220">but</w>
     <w lemma="to" pos="acp" xml:id="A94450-001-a-2230">to</w>
     <w lemma="very" pos="av" xml:id="A94450-001-a-2240">very</w>
     <w lemma="little" pos="j" xml:id="A94450-001-a-2250">little</w>
     <w lemma="consolation" pos="n1" xml:id="A94450-001-a-2260">consolation</w>
     <pc xml:id="A94450-001-a-2270">,</pc>
     <w lemma="for" pos="acp" xml:id="A94450-001-a-2280">for</w>
     <w join="right" lemma="there" pos="av" xml:id="A94450-001-a-2290">there</w>
     <w join="left" lemma="be" pos="vvz" xml:id="A94450-001-a-2291">'s</w>
     <w lemma="not" pos="xx" xml:id="A94450-001-a-2300">not</w>
     <w lemma="a" pos="d" xml:id="A94450-001-a-2310">a</w>
     <w lemma="word" pos="n1" xml:id="A94450-001-a-2320">word</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-2330">of</w>
     <w lemma="return" pos="vvg" xml:id="A94450-001-a-2340">returning</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-2350">his</w>
     <w lemma="coin" pos="n1" reg="coin" xml:id="A94450-001-a-2360">Coine</w>
     <w lemma="again" pos="av" xml:id="A94450-001-a-2370">again</w>
     <pc xml:id="A94450-001-a-2380">,</pc>
     <w lemma="but" pos="acp" xml:id="A94450-001-a-2390">but</w>
     <w lemma="a" pos="d" xml:id="A94450-001-a-2400">a</w>
     <w lemma="long" pos="j" xml:id="A94450-001-a-2410">long</w>
     <w lemma="frivolous" pos="j" xml:id="A94450-001-a-2420">frivolous</w>
     <w lemma="discourse" pos="n1" xml:id="A94450-001-a-2430">Discourse</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-2440">of</w>
     <w lemma="patience" pos="n1" xml:id="A94450-001-a-2450">Patience</w>
     <pc xml:id="A94450-001-a-2460">,</pc>
     <w lemma="which" pos="crq" xml:id="A94450-001-a-2470">which</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-2480">he</w>
     <w lemma="never" pos="avx" xml:id="A94450-001-a-2490">never</w>
     <w lemma="intend" pos="vvd" xml:id="A94450-001-a-2500">intended</w>
     <w lemma="for" pos="acp" xml:id="A94450-001-a-2510">for</w>
     <w lemma="himself" pos="pr" xml:id="A94450-001-a-2520">himself</w>
     <pc unit="sentence" xml:id="A94450-001-a-2530">.</pc>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-2540">And</w>
     <w lemma="oh" pos="uh" xml:id="A94450-001-a-2550">oh</w>
     <w lemma="that" pos="cs" xml:id="A94450-001-a-2560">that</w>
     <w lemma="one" pos="pi" xml:id="A94450-001-a-2570">one</w>
     <w lemma="ray" pos="n1" xml:id="A94450-001-a-2580">ray</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-2590">of</w>
     <w lemma="comfort" pos="n1" xml:id="A94450-001-a-2600">Comfort</w>
     <w lemma="more" pos="avc-d" xml:id="A94450-001-a-2610">more</w>
     <w lemma="will" pos="vmd" xml:id="A94450-001-a-2620">would</w>
     <w lemma="shine" pos="vvi" xml:id="A94450-001-a-2630">shine</w>
     <w lemma="forth" pos="av" xml:id="A94450-001-a-2640">forth</w>
     <w lemma="from" pos="acp" xml:id="A94450-001-a-2650">from</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-2660">the</w>
     <w lemma="bodkin" pos="n2" xml:id="A94450-001-a-2670">Bodkins</w>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-2680">and</w>
     <w lemma="thimble" pos="n2" xml:id="A94450-001-a-2690">Thimbles</w>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-2700">and</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-2710">the</w>
     <w lemma="bright" pos="j" xml:id="A94450-001-a-2720">bright</w>
     <w lemma="plate-candlestick" pos="n2" xml:id="A94450-001-a-2730">Plate-candlesticks</w>
     <w lemma="in" pos="acp" xml:id="A94450-001-a-2740">in</w>
     <hi xml:id="A94450-e140">
      <w lemma="Guildhall" pos="nn1" reg="Guildhall" xml:id="A94450-001-a-2750">Guild-hall</w>
      <pc xml:id="A94450-001-a-2760">,</pc>
     </hi>
     <w lemma="how" pos="crq" xml:id="A94450-001-a-2770">how</w>
     <w lemma="will" pos="vmd" xml:id="A94450-001-a-2780">would</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-2790">he</w>
     <w lemma="improve" pos="vvi" xml:id="A94450-001-a-2800">improve</w>
     <w lemma="such" pos="d" xml:id="A94450-001-a-2810">such</w>
     <w lemma="a" pos="d" xml:id="A94450-001-a-2820">an</w>
     <w lemma="advantage" pos="n1" xml:id="A94450-001-a-2830">advantage</w>
     <w lemma="to" pos="acp" xml:id="A94450-001-a-2840">to</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-2850">his</w>
     <w lemma="external" pos="j" xml:id="A94450-001-a-2860">external</w>
     <w lemma="happiness" pos="n1" xml:id="A94450-001-a-2870">happiness</w>
     <pc unit="sentence" xml:id="A94450-001-a-2880">!</pc>
     <w lemma="but" pos="acp" xml:id="A94450-001-a-2890">But</w>
     <w lemma="alas" pos="uh" xml:id="A94450-001-a-2900">alas</w>
     <w lemma="those" pos="d" xml:id="A94450-001-a-2910">those</w>
     <w lemma="silver" pos="n1" xml:id="A94450-001-a-2920">Silver</w>
     <w lemma="day" pos="n2" reg="days" xml:id="A94450-001-a-2930">daies</w>
     <w lemma="be" pos="vvb" xml:id="A94450-001-a-2940">are</w>
     <w lemma="do" pos="vvn" xml:id="A94450-001-a-2950">done</w>
     <pc xml:id="A94450-001-a-2960">,</pc>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-2970">and</w>
     <w lemma="this" pos="d" xml:id="A94450-001-a-2980">this</w>
     <w lemma="iron" pos="n1" xml:id="A94450-001-a-2990">iron</w>
     <w lemma="age" pos="n1" xml:id="A94450-001-a-3000">Age</w>
     <w lemma="have" pos="vvz" xml:id="A94450-001-a-3010">hath</w>
     <w lemma="overtake" pos="vvn" xml:id="A94450-001-a-3020">overtaken</w>
     <w lemma="your" pos="po" xml:id="A94450-001-a-3030">your</w>
     <w lemma="poor" pos="j" xml:id="A94450-001-a-3040">poor</w>
     <w lemma="petitioner" pos="n1" xml:id="A94450-001-a-3050">Petitioner</w>
     <pc unit="sentence" xml:id="A94450-001-a-3060">.</pc>
    </p>
    <p xml:id="A94450-e150">
     <w lemma="nevertheless" pos="av" xml:id="A94450-001-a-3070">Nevertheless</w>
     <pc xml:id="A94450-001-a-3080">,</pc>
     <w lemma="in" pos="acp" xml:id="A94450-001-a-3090">in</w>
     <w lemma="regard" pos="n1" xml:id="A94450-001-a-3100">regard</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-3110">of</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-3120">his</w>
     <w lemma="service" pos="n1" xml:id="A94450-001-a-3130">Service</w>
     <w lemma="at" pos="acp" xml:id="A94450-001-a-3140">at</w>
     <hi xml:id="A94450-e160">
      <w lemma="Leistithiel" pos="nn1" xml:id="A94450-001-a-3150">Leistithiel</w>
      <pc xml:id="A94450-001-a-3160">,</pc>
     </hi>
     <w lemma="where" pos="crq" xml:id="A94450-001-a-3170">where</w>
     <w lemma="he" pos="pns" xml:id="A94450-001-a-3180">he</w>
     <w lemma="alone" pos="av-j" xml:id="A94450-001-a-3190">alone</w>
     <w lemma="despair" pos="vvd" xml:id="A94450-001-a-3200">despaired</w>
     <w lemma="not" pos="xx" xml:id="A94450-001-a-3210">not</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-3220">of</w>
     <w lemma="your" pos="po" xml:id="A94450-001-a-3230">your</w>
     <w lemma="cause" pos="n1" xml:id="A94450-001-a-3240">Cause</w>
     <pc join="right" xml:id="A94450-001-a-3250">(</pc>
     <w lemma="for" pos="acp" xml:id="A94450-001-a-3260">for</w>
     <w lemma="Essex" pos="nn1" rend="hi" xml:id="A94450-001-a-3270">Essex</w>
     <w lemma="have" pos="vvd" xml:id="A94450-001-a-3280">had</w>
     <w lemma="leave" pos="vvn" xml:id="A94450-001-a-3290">left</w>
     <w lemma="he" pos="pno" xml:id="A94450-001-a-3300">him</w>
     <w lemma="in" pos="acp" xml:id="A94450-001-a-3310">in</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-3320">the</w>
     <w lemma="lurch" pos="n1" xml:id="A94450-001-a-3330">lurch</w>
     <pc xml:id="A94450-001-a-3340">,</pc>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-3350">and</w>
     <w join="right" lemma="it" pos="pn" xml:id="A94450-001-a-3360">'t</w>
     <w join="left" lemma="be" pos="vvd" xml:id="A94450-001-a-3361">was</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-3370">his</w>
     <w lemma="best" pos="js" xml:id="A94450-001-a-3380">best</w>
     <w lemma="course" pos="n1" xml:id="A94450-001-a-3390">course</w>
     <w lemma="to" pos="prt" xml:id="A94450-001-a-3400">to</w>
     <w lemma="show" pos="vvi" reg="show" xml:id="A94450-001-a-3410">shew</w>
     <w lemma="confidence" pos="n1" xml:id="A94450-001-a-3420">confidence</w>
     <pc xml:id="A94450-001-a-3430">)</pc>
     <w lemma="be" pos="vvb" xml:id="A94450-001-a-3440">be</w>
     <w lemma="please" pos="vvn" xml:id="A94450-001-a-3450">pleased</w>
     <w lemma="to" pos="prt" xml:id="A94450-001-a-3460">to</w>
     <w lemma="consider" pos="vvi" xml:id="A94450-001-a-3470">consider</w>
     <w lemma="he" pos="pno" xml:id="A94450-001-a-3480">him</w>
     <w lemma="in" pos="acp" xml:id="A94450-001-a-3490">in</w>
     <w lemma="this" pos="d" xml:id="A94450-001-a-3500">this</w>
     <w lemma="his" pos="po" xml:id="A94450-001-a-3510">his</w>
     <w lemma="distress" pos="n1" xml:id="A94450-001-a-3520">distress</w>
     <pc xml:id="A94450-001-a-3530">,</pc>
     <w lemma="and" pos="cc" xml:id="A94450-001-a-3540">and</w>
     <w lemma="befriend" pos="vvi" xml:id="A94450-001-a-3550">befriend</w>
     <w lemma="he" pos="pno" xml:id="A94450-001-a-3560">him</w>
     <w lemma="in" pos="acp" xml:id="A94450-001-a-3570">in</w>
     <w lemma="these" pos="d" xml:id="A94450-001-a-3580">these</w>
     <w lemma="follow" pos="j-vg" xml:id="A94450-001-a-3590">following</w>
     <w lemma="request" pos="n2" xml:id="A94450-001-a-3600">Requests</w>
     <pc unit="sentence" xml:id="A94450-001-a-3610">.</pc>
    </p>
    <p xml:id="A94450-e180">
     <hi xml:id="A94450-e190">
      <w lemma="that" pos="cs" xml:id="A94450-001-a-3620">That</w>
      <w lemma="you" pos="pn" xml:id="A94450-001-a-3630">You</w>
      <w lemma="will" pos="vmd" xml:id="A94450-001-a-3640">would</w>
      <w lemma="be" pos="vvi" xml:id="A94450-001-a-3650">be</w>
      <w lemma="please" pos="vvn" xml:id="A94450-001-a-3660">pleased</w>
      <w lemma="by" pos="acp" xml:id="A94450-001-a-3670">by</w>
      <w lemma="interpose" pos="vvg" xml:id="A94450-001-a-3680">interposing</w>
      <w lemma="your" pos="po" xml:id="A94450-001-a-3690">Your</w>
      <w lemma="powerful" pos="j" reg="powerful" xml:id="A94450-001-a-3700">powerfull</w>
      <w lemma="assistance" pos="n1" xml:id="A94450-001-a-3710">assistance</w>
      <w lemma="with" pos="acp" xml:id="A94450-001-a-3720">with</w>
      <w lemma="the" pos="d" xml:id="A94450-001-a-3730">the</w>
      <w lemma="governor" pos="n2" reg="governors" xml:id="A94450-001-a-3740">Governours</w>
      <w lemma="of" pos="acp" xml:id="A94450-001-a-3750">of</w>
     </hi>
     <w lemma="sutton" pos="nng1" reg="Sutton's" xml:id="A94450-001-a-3760">Suttons</w>
     <hi xml:id="A94450-e200">
      <w lemma="hospital" pos="n1" xml:id="A94450-001-a-3770">Hospital</w>
      <pc xml:id="A94450-001-a-3780">,</pc>
      <w lemma="to" pos="prt" xml:id="A94450-001-a-3790">to</w>
      <w lemma="get" pos="vvi" xml:id="A94450-001-a-3800">get</w>
      <w lemma="he" pos="pno" xml:id="A94450-001-a-3810">him</w>
      <w lemma="admit" pos="vvn" xml:id="A94450-001-a-3820">admitted</w>
      <w lemma="a" pos="d" xml:id="A94450-001-a-3830">a</w>
      <w lemma="pensioner" pos="n1" reg="pensioner" xml:id="A94450-001-a-3840">Pentioner</w>
      <w lemma="there" pos="av" xml:id="A94450-001-a-3850">there</w>
      <pc xml:id="A94450-001-a-3860">;</pc>
      <w lemma="or" pos="cc" xml:id="A94450-001-a-3870">or</w>
      <w lemma="rather" pos="avc" xml:id="A94450-001-a-3880">rather</w>
      <w lemma="that" pos="cs" xml:id="A94450-001-a-3890">that</w>
      <w lemma="you" pos="pn" xml:id="A94450-001-a-3900">You</w>
      <w lemma="will" pos="vmd" xml:id="A94450-001-a-3910">would</w>
      <w lemma="please" pos="vvi" xml:id="A94450-001-a-3920">please</w>
      <w lemma="to" pos="prt" xml:id="A94450-001-a-3930">to</w>
      <w lemma="obtain" pos="vvi" xml:id="A94450-001-a-3940">obtain</w>
      <w lemma="for" pos="acp" xml:id="A94450-001-a-3950">for</w>
      <w lemma="he" pos="pno" xml:id="A94450-001-a-3960">him</w>
      <w lemma="from" pos="acp" xml:id="A94450-001-a-3970">from</w>
      <w lemma="the" pos="d" xml:id="A94450-001-a-3980">the</w>
      <w lemma="parliament" pos="n1" xml:id="A94450-001-a-3990">Parliament</w>
      <w lemma="the" pos="d" xml:id="A94450-001-a-4000">the</w>
      <w lemma="next" pos="ord" xml:id="A94450-001-a-4010">next</w>
      <w lemma="vacancy" pos="n1" xml:id="A94450-001-a-4020">vacancy</w>
      <w lemma="of" pos="acp" xml:id="A94450-001-a-4030">of</w>
      <w lemma="a" pos="d" xml:id="A94450-001-a-4040">a</w>
      <w lemma="poor" pos="j" reg="poor" xml:id="A94450-001-a-4050">poore</w>
     </hi>
     <w lemma="knight" pos="n1" xml:id="A94450-001-a-4060">Knight</w>
     <w lemma="at" pos="acp" xml:id="A94450-001-a-4070">at</w>
     <w lemma="Windsor" pos="nn1" xml:id="A94450-001-a-4080">Windsor</w>
     <pc xml:id="A94450-001-a-4090">;</pc>
     <hi xml:id="A94450-e210">
      <w lemma="otherwise" pos="av" xml:id="A94450-001-a-4100">otherwise</w>
      <w lemma="your" pos="po" xml:id="A94450-001-a-4110">your</w>
      <w lemma="petitioner" pos="n1" xml:id="A94450-001-a-4120">Petitioner</w>
      <pc xml:id="A94450-001-a-4130">,</pc>
      <w lemma="for" pos="acp" xml:id="A94450-001-a-4140">for</w>
      <w lemma="all" pos="d" xml:id="A94450-001-a-4150">all</w>
      <w lemma="general" pos="j" xml:id="A94450-001-a-4160">General</w>
     </hi>
     <w lemma="monk" pos="n2" reg="monks" xml:id="A94450-001-a-4170">Moncks</w>
     <hi xml:id="A94450-e220">
      <w lemma="detestation" pos="n1" xml:id="A94450-001-a-4180">detestation</w>
      <w lemma="of" pos="acp" xml:id="A94450-001-a-4190">of</w>
      <w lemma="oath" pos="n2" xml:id="A94450-001-a-4200">Oaths</w>
      <w lemma="and" pos="cc" xml:id="A94450-001-a-4210">and</w>
      <w lemma="abjuration" pos="n1" xml:id="A94450-001-a-4220">Abjuration</w>
      <pc xml:id="A94450-001-a-4230">,</pc>
      <w lemma="will" pos="vmb" xml:id="A94450-001-a-4240">will</w>
      <w lemma="set" pos="vvi" xml:id="A94450-001-a-4250">set</w>
      <w lemma="up" pos="acp" xml:id="A94450-001-a-4260">up</w>
      <w lemma="for" pos="acp" xml:id="A94450-001-a-4270">for</w>
      <w lemma="himself" pos="pr" xml:id="A94450-001-a-4280">himself</w>
      <w lemma="a" pos="d" xml:id="A94450-001-a-4290">a</w>
     </hi>
     <w lemma="knight" pos="n1" xml:id="A94450-001-a-4300">Knight</w>
     <w lemma="of" pos="acp" xml:id="A94450-001-a-4310">of</w>
     <w lemma="the" pos="d" xml:id="A94450-001-a-4320">the</w>
     <w lemma="post" pos="n1" xml:id="A94450-001-a-4330">Post</w>
     <pc xml:id="A94450-001-a-4340">,</pc>
     <hi xml:id="A94450-e230">
      <w join="right" lemma="that" pos="d" xml:id="A94450-001-a-4350">that</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A94450-001-a-4351">'s</w>
      <w lemma="a" pos="d" xml:id="A94450-001-a-4360">a</w>
      <w lemma="military" pos="j" xml:id="A94450-001-a-4370">Military</w>
      <w lemma="as" pos="acp" xml:id="A94450-001-a-4380">as</w>
      <w lemma="well" pos="av" xml:id="A94450-001-a-4390">well</w>
      <w lemma="as" pos="acp" xml:id="A94450-001-a-4400">as</w>
      <w lemma="civil" pos="j" xml:id="A94450-001-a-4410">Civil</w>
      <w lemma="term" pos="n1" reg="term" xml:id="A94450-001-a-4420">terme</w>
      <pc xml:id="A94450-001-a-4430">,</pc>
      <w lemma="and" pos="cc" xml:id="A94450-001-a-4440">and</w>
      <w lemma="will" pos="vmb" xml:id="A94450-001-a-4450">will</w>
      <w lemma="serve" pos="vvi" xml:id="A94450-001-a-4460">serve</w>
      <w lemma="to" pos="prt" xml:id="A94450-001-a-4470">to</w>
      <w lemma="compensate" pos="vvi" xml:id="A94450-001-a-4480">compensate</w>
      <w lemma="his" pos="po" xml:id="A94450-001-a-4490">his</w>
      <w lemma="lose" pos="j-vn" xml:id="A94450-001-a-4500">lost</w>
      <w lemma="command" pos="n1" xml:id="A94450-001-a-4510">Command</w>
      <w lemma="of" pos="acp" xml:id="A94450-001-a-4520">of</w>
      <w lemma="major" pos="j" xml:id="A94450-001-a-4530">Major</w>
      <w lemma="general" pos="n1" xml:id="A94450-001-a-4540">General</w>
      <pc unit="sentence" xml:id="A94450-001-a-4550">.</pc>
     </hi>
    </p>
    <closer xml:id="A94450-e240">
     <w lemma="and" pos="cc" xml:id="A94450-001-a-4560">And</w>
     <w lemma="your" pos="po" xml:id="A94450-001-a-4570">your</w>
     <w lemma="petitioner" pos="n1" xml:id="A94450-001-a-4580">Petitioner</w>
     <w lemma="shall" pos="vmb" xml:id="A94450-001-a-4590">shall</w>
     <w lemma="pray" pos="vvi" xml:id="A94450-001-a-4600">pray</w>
     <pc xml:id="A94450-001-a-4610">,</pc>
     <w lemma="etc." pos="ab" xml:id="A94450-001-a-4620">&amp;c.</w>
     <pc unit="sentence" xml:id="A94450-001-a-4630"/>
    </closer>
   </div>
  </body>
  <back xml:id="A94450-e250">
   <div type="colophon" xml:id="A94450-e260">
    <p xml:id="A94450-e270">
     <hi xml:id="A94450-e280">
      <w lemma="LONDON" pos="nn1" xml:id="A94450-001-a-4640">LONDON</w>
      <pc xml:id="A94450-001-a-4650">:</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A94450-001-a-4660">Printed</w>
     <w lemma="for" pos="acp" xml:id="A94450-001-a-4670">for</w>
     <hi xml:id="A94450-e290">
      <w lemma="William" pos="nn1" xml:id="A94450-001-a-4680">William</w>
      <w lemma="Waterson" pos="nn1" xml:id="A94450-001-a-4690">Waterson</w>
      <pc unit="sentence" xml:id="A94450-001-a-4700">.</pc>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
